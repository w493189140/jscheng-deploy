# jscheng-deploy
本工具是修改 fuchengwei 的 deploy-cli-service 
# github

[https://gitee.com/w493189140/jscheng-deploy](https://gitee.com/w493189140/jscheng-deploy)

# npm

[https://www.npmjs.com/package/jscheng-deploy](https://www.npmjs.com/package/jscheng-deploy)

# 安装

本地安装 jscheng-deploy

```shell
cnpm install jscheng-deploy --save-dev
```

查看版本，表示安装成功

```shell
npx jscheng-deploy -v
```
# 使用

#### 1.查看帮助

```shell
npx jscheng-deploy -h
```

#### 2.初始化配置文件（在项目目录下）

```shell
npx jscheng-deploy init # 或者使用简写 npx deploy-cli-service i
```

根据提示填写内容，会在项目根目录下生成 `deploy.config.js` 文件，初始化配置只会生成 `dev` (开发环境)、`test` (测试环境)、`prod` (生产环境) 三个配置，再有其他配置可参考模板自行配置。


#### 3.手动创建或修改配置文件

在项目根目录下手动创建 `deploy.config.js` 文件，复制以下代码按情况修改即可。

```javascript
module.exports = {
  projectName: 'test', // 项目名称
  dev: {
    // 环境对象
    name: 'dev环境', // 环境名称
    script: 'npm run build:dev', // 打包命令
    host: '47.98.175.99', // 服务器地址
    port: 22, // 服务器端口号
    username: 'root', // 服务器登录用户名
    password: '***', // 服务器登录密码 *** 代表发布时输入 为了安全尽量***
    distPath: 'dist', // 本地打包生成目录
    webDir: '/web/webapps/student/dist', // 服务器部署路径(不可为空或'/')
    isDelLocal: false,//上线之后是否删除本地文件dist
    projectUrl: 'https://m.jscheng.top' //项目上线后的地址
  },
  test: {
    // 环境对象
    name: '测试环境', // 环境名称
    script: 'npm run build:test', // 打包命令
    host: '192.168.0.1', // 服务器地址
    port: 22, // 服务器端口号
    username: 'root', // 服务器登录用户名
    password: '***', // 服务器登录密码 *** 代表发布时输入 为了安全尽量***
    distPath: 'dist', // 本地打包生成目录
    webDir: '/web/webapps/student/dist', // 服务器部署路径(不可为空或'/')
    isDelLocal: false,//上线之后是否删除本地文件dist
    projectUrl: 'https://m.jscheng.top' //项目上线后的地址
  },
  prod: {
    // 环境对象
    name: '开发环境', // 环境名称
    script: 'npm run build', // 打包命令
    host: '47.98.175.99', // 服务器地址
    port: 22, // 服务器端口号
    username: 'root', // 服务器登录用户名
    password: '***', // 服务器登录密码 *** 代表发布时输入 为了安全尽量***
    distPath: 'dist', // 本地打包生成目录
    webDir: '/web/webapps/student/dist', // 服务器部署路径(不可为空或'/')
    isDelLocal: false,//上线之后是否删除本地文件dist
    projectUrl: 'https://m.jscheng.top' //项目上线后的地址
  }
}
```

#### 4.部署 （在项目目录下）

注意：命令后面需要加 `--mode` 环境对象 （如：`--mode dev`）

```shell
npx jscheng-deploy deploy --mode dev # 或者使用 npx jscheng-deploy d --mode dev
```

输入 `Y` 确认后即可开始自动部署，看见如下提示说明部署完成


#### 5.本地安装扩展

如果使用本地安装命令的话，可以在项目根目录下的 `package.json` 文件中 `scripts` 脚本中添加如下代码

```
"deploy": "jscheng-deploy deploy --mode prod"
```

然后使用下面代码也可以完成部署操作

```shell
npm run deploy:dev
```
